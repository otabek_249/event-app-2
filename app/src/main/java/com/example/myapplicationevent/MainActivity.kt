package com.example.myapplicationevent

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.wifi.WifiManager
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplicationevent.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPreferences: SharedPreferences
    private val PREF_NAME = "my_pref"
    private var pilotBol = true
    private var bluetoothOnSwitchBol = true
    private var powerSwitchBol = true
    private var lowBatterySwitchBol = true
    private var wifiOnSwitchBol = true
    private var internetBol = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        // Restore switch states from SharedPreferences
        pilotBol = sharedPreferences.getBoolean("pilotBol", true)
        bluetoothOnSwitchBol = sharedPreferences.getBoolean("bluetoothOnSwitchBol", true)
        powerSwitchBol = sharedPreferences.getBoolean("powerSwitchBol", true)
        lowBatterySwitchBol = sharedPreferences.getBoolean("lowBatterySwitchBol", true)
        wifiOnSwitchBol = sharedPreferences.getBoolean("wifiOnSwitchBol", true)
        internetBol = sharedPreferences.getBoolean("internetBol", true)
        setContentView(binding.root)
        startMyNotificationService()

        setupClickListeners()
    }

    private fun setupClickListeners() {
        with(binding) {
            airplaneSwitch.setOnClickListener {
                pilotBol = !pilotBol
                updateSwitchState()
            }

            bluetoothOnSwitch.setOnClickListener {
                bluetoothOnSwitchBol = !bluetoothOnSwitchBol
                updateSwitchState()
            }

            powerSwitch.setOnClickListener {
                powerSwitchBol = !powerSwitchBol
                updateSwitchState()
            }

            lowBatterySwitch.setOnClickListener {
                lowBatterySwitchBol = !lowBatterySwitchBol
                updateSwitchState()
            }

            wifiOnSwitch.setOnClickListener {
                wifiOnSwitchBol = !wifiOnSwitchBol
                updateSwitchState()
            }

            internetSwitch.setOnClickListener {
                internetBol = !internetBol
                updateSwitchState()
            }
        }
    }

    private fun startMyNotificationService() {
        Log.d("TTT","startMyNotificationService")
        val intent = Intent(this@MainActivity, MyNotificationService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d("TTT","startMyNotificationService")
            startForegroundService(intent)
        } else {
            Log.d("TTT","startMyNotificationService")
            startService(intent)
        }
    }

    private fun updateSwitchState() {
        with(binding) {
            airplaneSwitch.setImageResource(if (pilotBol) R.drawable.switch_on else R.drawable.switch_off)
            bluetoothOnSwitch.setImageResource(if (bluetoothOnSwitchBol) R.drawable.switch_on else R.drawable.switch_off)
            powerSwitch.setImageResource(if (powerSwitchBol) R.drawable.switch_on else R.drawable.switch_off)
            lowBatterySwitch.setImageResource(if (lowBatterySwitchBol) R.drawable.switch_on else R.drawable.switch_off)
            wifiOnSwitch.setImageResource(if (wifiOnSwitchBol) R.drawable.switch_on else R.drawable.switch_off)
            internetSwitch.setImageResource(if (internetBol) R.drawable.switch_on else R.drawable.switch_off)
            registerReceivers()
        }
    }

    private fun registerReceivers() {
        val intentFilter = IntentFilter().apply {
            if (bluetoothOnSwitchBol) {
                addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED)
            }
            if (pilotBol) {
                addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)
            }
            if (powerSwitchBol) {
                addAction(BatteryManager.ACTION_CHARGING)
                addAction(BatteryManager.ACTION_DISCHARGING)
            }
            if (wifiOnSwitchBol) {
                addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
            }
            if (internetBol) {
                addAction("android.net.conn.CONNECTIVITY_CHANGE")
            }
        }
        registerReceiver(TTMyNotificationReceiver(), intentFilter)
    }

    override fun onStart() {
        super.onStart()
        registerReceivers()
    }

    override fun onStop() {
        super.onStop()
        unregisterReceivers()
    }

    private fun unregisterReceivers() {
        unregisterReceiver(TTMyNotificationReceiver())
    }

    private fun saveSwitchStates() {
        val editor = sharedPreferences.edit()
        editor.putBoolean("pilotBol", pilotBol)
        editor.putBoolean("bluetoothOnSwitchBol", bluetoothOnSwitchBol)
        editor.putBoolean("powerSwitchBol", powerSwitchBol)
        editor.putBoolean("lowBatterySwitchBol", lowBatterySwitchBol)
        editor.putBoolean("wifiOnSwitchBol", wifiOnSwitchBol)
        editor.putBoolean("internetBol", internetBol)
        editor.apply()
    }

    override fun onPause() {
        super.onPause()
        // Save switch states when the activity is paused
        saveSwitchStates()
    }
}
