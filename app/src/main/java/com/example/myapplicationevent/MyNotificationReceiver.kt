package com.example.myapplicationevent

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.BatteryManager
import android.util.Log
import com.example.myapplicationevent.batteryLow
import com.example.myapplicationevent.batteryTurnOn
import com.example.myapplicationevent.bluetoothTurnOff
import com.example.myapplicationevent.bluetoothTurnOn
import com.example.myapplicationevent.internetTurnOn
import com.example.myapplicationevent.pilotTurnOn
import com.example.myapplicationevent.setServiceActions
import com.example.myapplicationevent.wifiTurnOff
import com.example.myapplicationevent.wifiTurnOn


class TTMyNotificationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        Log.d("BAHA", "onRecive: ${intent.action}")

        when (intent.action) {
            BatteryManager.ACTION_CHARGING -> {
                setServiceActions?.invoke(batteryTurnOn)
            }

            BatteryManager.ACTION_DISCHARGING -> {
                setServiceActions?.invoke(batteryLow)
            }

            Intent.ACTION_AIRPLANE_MODE_CHANGED -> {
                setServiceActions?.invoke(pilotTurnOn)
            }

            WifiManager.WIFI_STATE_CHANGED_ACTION -> {
                val wifeStataExtra =
                    intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN)
                when (wifeStataExtra) {
                    WifiManager.WIFI_STATE_ENABLING -> {
                        setServiceActions?.invoke(wifiTurnOn)
                    }

                    WifiManager.WIFI_STATE_DISABLING -> {
                        setServiceActions?.invoke(wifiTurnOff)
                    }
                }
            }

            BluetoothAdapter.ACTION_STATE_CHANGED -> {
                val bluetoothState =
                    intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)
                when (bluetoothState) {
                    BluetoothAdapter.STATE_ON -> {
                        setServiceActions?.invoke(bluetoothTurnOn)
                    }

                    BluetoothAdapter.STATE_OFF -> {
                        setServiceActions?.invoke(bluetoothTurnOff)
                    }
                }
            }

            "android.net.conn.CONNECTIVITY_CHANGE" -> {
                setServiceActions?.invoke(internetTurnOn)
            }
        }
    }
}