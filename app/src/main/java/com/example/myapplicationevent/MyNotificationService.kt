package com.example.myapplicationevent

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.example.myapplicationevent.R
import com.example.myapplicationevent.batteryLow
import com.example.myapplicationevent.batteryTurnOn
import com.example.myapplicationevent.bluetoothTurnOff
import com.example.myapplicationevent.bluetoothTurnOn
import com.example.myapplicationevent.internetTurnOn
import com.example.myapplicationevent.pilotTurnOn
import com.example.myapplicationevent.setServiceActions
import com.example.myapplicationevent.wifiTurnOff
import com.example.myapplicationevent.wifiTurnOn


class MyNotificationService : Service() {

    private val notificationId = 1
    private val channel = "my_chanel"


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("TTT","oncreate:")
        setServiceActions = {
            var mediaPlayer = MediaPlayer()
            when (it) {
                batteryTurnOn -> {
                    Log.d("TTT","oncreate: $it")
                    mediaPlayer = MediaPlayer.create(this, R.raw.battery_on)
                }

                batteryLow -> {
                    Log.d("TTT","oncreate: $it")
                     mediaPlayer = MediaPlayer.create(this, R.raw.battery_off)
                }

                wifiTurnOn -> {
                    Log.d("TTT","oncreate: $it")
                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_yondi)
                }

                wifiTurnOff -> {
                    Log.d("TTT","oncreate: $it")
                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_uchdi)
                }

                bluetoothTurnOn -> {
                    Log.d("TTT","oncreate: $it")
                    mediaPlayer = MediaPlayer.create(this, R.raw.bluetooth_on)
                }

                bluetoothTurnOff -> {
                    Log.d("TTT","oncreate: $it")
                    mediaPlayer = MediaPlayer.create(this, R.raw.bluetooth_off)
                }

                internetTurnOn -> {
                    mediaPlayer = if (isOnline()) {
                        Log.d("TTT","oncreate: $it")
                        MediaPlayer.create(this, R.raw.internet_on)
                    } else {
                        Log.d("TTT","oncreate: $it")
                        MediaPlayer.create(this, R.raw.internet_off)
                    }
                }

                pilotTurnOn -> {
                    mediaPlayer = MediaPlayer.create(this, R.raw.airplane_on)
                }
            }
            mediaPlayer.start()
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "Servise Create bo'ldi", Toast.LENGTH_LONG).show()
        startForeground(notificationId, showNotification())
        return super.onStartCommand(intent, flags, startId)
    }

    private fun showNotification(): Notification {
        return NotificationCompat
            .Builder(this, channel)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Broadcast app")
            .setContentText("O'zgarishlarni bilish uchun bildirishnomani o'chirmang!")
            .build()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun isOnline(): Boolean {
        val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }
}