package com.example.myapplicationevent

var setServiceActions: ((String) -> Unit)? = null
const val wifiTurnOff = "WIFE_TURN_OFF"
const val wifiTurnOn = "WIFE_TURN_ON"
const val pilotTurnOff = "PILOT_TURN_OF"
const val pilotTurnOn = "PILOT_TURN_ON"
const val internetTurnOn = "INTERNET_TURN_ON"
const val internetTurnOff = "INTERNET_TURN_OF"
const val batteryTurnOn = "BATTERY_TURN_ON"
const val batteryLow = "BATTERY_LOW"
const val bluetoothTurnOn = "BLUETOOTH_TURN_ON"
const val bluetoothTurnOff = "BLUETOOTH_TURN_OF"